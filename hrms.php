<?php
/*
Plugin Name: Polcode Hrms
Plugin URI: https://polcode.com/hrms
Description: HR support plugin
Author: Polcode dev team
Version: 0.1
Author URI: https://polcode.com/
*/

/**
 * set of initial configuration, to improve UX after installing plugin
 */
define('HRMS_SETUP_REQUIRED', 'hrms_setup_required_flag');


global $wpdb;
/**
 * constants
 */
define('HRMS_URL', 'hrms');
define('HRMS_URL_PREFIX', 'hrms/');
define('HRMS_USER_ROLE', 'hrms_user');
define('HRMS_ADMIN_ACL_ROLE', 'HRMS Admin');
define('HRMS_USER_DELETED_META', 'hrms_user_deleted');
define('HRMS_WP_DB_TABLE_PREFIX', $wpdb->prefix);
define('HRMS_DB_TABLE_PREFIX', $wpdb->prefix . 'hrms_');

require_once dirname(__FILE__) . '/wp_ish/hrms_wp_ish.php';
$wp_ish = new hrmsWpish();

add_action('init', array($wp_ish, 'init_actions'));

add_action('delete_user', array($wp_ish, 'delete_user'));

add_action('user_register', array($wp_ish, 'add_empty_delete_flag'), 99, 1);

add_filter('login_redirect', array($wp_ish, 'login_redirect'), 99, 3);

add_filter('manage_users_columns', array($wp_ish, 'new_modify_user_table'));
add_filter('manage_users_custom_column', array($wp_ish, 'new_modify_user_table_row'), 10, 3);

register_activation_hook(__FILE__, 'hrms_activate');
register_deactivation_hook(__FILE__, 'hrms_deactivate');

// script to process background tasks
require_once 'wp_ish/wp_bpu/wp-background-processing.php';

function hrms_activate()
{
    global $wp_rewrite;
    require_once dirname(__FILE__) . '/hrms_loader.php';
    $loader = new HrmsLoader();
    $loader->activate();
    $wp_rewrite->flush_rules(true);

    add_role(HRMS_USER_ROLE, __('HRMS User', 'hrms'), array(
        'subscriber'   => true,
        'upload_files' => true,
    ));

    //check if app needs setup
    //get app users
    $users = get_users(array('role' => HRMS_USER_ROLE));
    if (empty($users)) {
        update_option(HRMS_SETUP_REQUIRED, 1);
    }else{
        update_option(HRMS_SETUP_REQUIRED, 0);
    }

    //add empty delete flag to all users
    $all_users = get_users(array('fields' => array('display_name')));
    foreach ($all_users as $user_id => $user) {
        add_user_meta($user_id, HRMS_USER_DELETED_META, '0', true);
    }


}

function hrms_deactivate()
{
    global $wp_rewrite;
    require_once dirname(__FILE__) . '/hrms_loader.php';
    $loader = new HrmsLoader();
    $loader->deactivate();
    $wp_rewrite->flush_rules(true);

    //@ToDo decide whether to remove user role HRMS_USER_ROLE
}

?>